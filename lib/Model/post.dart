import 'dart:convert';
import 'package:http/http.dart' as http;

class Post {
  String? name;
  String? job;

  Post({this.name, this.job});
  factory Post.createPostResult(Map<String, dynamic> object) {
    return Post(
      name: object['name'],
      job: object['job'],
    );
  }

  static Future<Post> postDataUser(String name, String job) async {
    String apiURL = 'https://reqres.in/api/users';

    var getResultData =
        await http.post(Uri.parse(apiURL), body: {'name': name, 'job': job});
    var jsonObject = json.decode(getResultData.body);
    return Post.createPostResult(jsonObject);
  }
}
