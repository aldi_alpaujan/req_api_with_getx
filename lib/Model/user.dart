import 'package:http/http.dart' as http;
import 'dart:convert';

class User {
  String? id;
  String? nama;
  String? email;
  String? avatar;

  User({this.id, this.nama, this.email, this.avatar});

  factory User.createDataUser(Map<String, dynamic> object) {
    return User(
      id: object['id'].toString(),
      nama: object['first_name'] + object['last_name'],
      email: object['email'],
      avatar: object['avatar'],
    );
  }

  static Future<User> getDataFromApi(int id) async {
    String apiURL = 'https://reqres.in/api/users/' + id.toString();

    var apiResult = await http.get(Uri.parse(apiURL));
    var jsonObject = json.decode(apiResult.body);
    var userData = (jsonObject as Map<String, dynamic>)['data'];

    return User.createDataUser(userData);
  }
}
