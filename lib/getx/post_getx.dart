import 'package:get/get.dart';
import 'package:getx_api/Model/post.dart';

class PostController extends GetxController {
  var data = Post().obs;
  void postData(String name, String job) {
    Post.postDataUser(name, job).then((value) => data.value = value);
  }
}
