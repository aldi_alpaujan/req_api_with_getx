import 'dart:math';

import 'package:get/get.dart';
import 'package:getx_api/Model/user.dart';

class UserController extends GetxController {
  User user = User();
  Random r = Random();

  void pickData() {
    User.getDataFromApi(r.nextInt(10) + 1).then((value) => user = value);
    update();
  }
}
