import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_api/getx/user_getx.dart';
import 'package:getx_api/view/widget/user_card.dart';

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // var userC = Get.put(UserController());
    return Scaffold(
      appBar:
          AppBar(title: const Center(child: Text('Demo Get API with GetX'))),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  Get.find<UserController>().pickData();
                },
                child: const Text('Pick Data')),
            const SizedBox(height: 5),
            GetBuilder<UserController>(
              init: UserController(),
              builder: (controller) =>
                  (controller.user.id == null) ? Container() : const UserCard(),
            )
          ],
        ),
      ),
    );
  }
}
