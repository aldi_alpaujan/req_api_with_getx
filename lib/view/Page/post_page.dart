import 'package:flutter/material.dart';
import 'package:getx_api/getx/post_getx.dart';
import 'package:getx_api/view/widget/card_result.dart';
import 'package:get/get.dart';
import 'package:getx_api/view/widget/text_field.dart';

class PostPage extends StatelessWidget {
  const PostPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var postC = Get.put(PostController());
    var name = TextEditingController().obs;
    var job = TextEditingController().obs;

    return Scaffold(
      appBar: AppBar(title: const Center(child: Text('Post Demo with GetX'))),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextFieldPost(labelTxt: 'Name', textController: name.value),
            const SizedBox(height: 10),
            TextFieldPost(labelTxt: 'Job', textController: job.value),
            SizedBox(
                width: 100,
                child: ElevatedButton(
                    onPressed: () {
                      if (name.value.text != '' && job.value.text != '') {
                        postC.postData(name.value.text, job.value.text);
                      }
                      name.value.text = '';
                      job.value.text = '';
                    },
                    child: const Text("Post"))),
            const SizedBox(height: 10),
            Obx(() => (postC.data.value.name == null)
                ? Container()
                : const ResultCard()),
          ],
        ),
      ),
    );
  }
}
