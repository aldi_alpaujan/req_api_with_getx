import 'package:flutter/material.dart';

class TextFieldPost extends StatelessWidget {
  final bool obscureText;
  final String? labelTxt;
  final TextEditingController textController;
  const TextFieldPost(
      {Key? key,
      this.obscureText = false,
      required this.labelTxt,
      required this.textController})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(15)),
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        labelText: labelTxt,
      ),
      obscureText: obscureText,
      controller: textController,
    );
  }
}
