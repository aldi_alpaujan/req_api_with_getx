import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_api/getx/user_getx.dart';

class UserCard extends StatelessWidget {
  const UserCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      height: 100,
      width: 350,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: const [
          BoxShadow(
            blurRadius: 3,
            offset: Offset(1, 1),
            color: Colors.grey,
          ),
        ],
      ),
      child: GetBuilder<UserController>(
        init: UserController(),
        builder: (controller) => Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.all(5),
              height: 90,
              width: 90,
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(15),
                  image: DecorationImage(
                      image: NetworkImage(controller.user.avatar.toString()))),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Id : ${controller.user.id}'),
                  Text('Name : ${controller.user.nama} '),
                  Text('Email : ${controller.user.email}'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
